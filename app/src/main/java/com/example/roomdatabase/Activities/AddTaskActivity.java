package com.example.roomdatabase.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.roomdatabase.Database.DatabaseClient;
import com.example.roomdatabase.Entity.Task;
import com.example.roomdatabase.R;
import com.example.roomdatabase.databinding.ActivityAddTaskBinding;


public class AddTaskActivity extends AppCompatActivity {


    ActivityAddTaskBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_add_task);



       binding.buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveTask();
            }
        });
    }

    private void saveTask() {
        final String sTask = binding.editTextTask.getText().toString().trim();
        final String sDesc = binding.editTextDesc.getText().toString().trim();
        final String sFinishBy = binding.editTextFinishBy.getText().toString().trim();

        if (sTask.isEmpty()) {
            binding.editTextTask.setError("Task required");
            binding.editTextTask.requestFocus();
            return;
        }

        if (sDesc.isEmpty()) {
            binding.editTextDesc.setError("Desc required");
            binding.editTextDesc.requestFocus();
            return;
        }

        if (sFinishBy.isEmpty()) {
            binding.editTextFinishBy.setError("Finish by required");
            binding.editTextFinishBy.requestFocus();
            return;
        }

        class SaveTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                //creating a task
                Task task = new Task();
                task.setTask(sTask);
                task.setDesc(sDesc);
                task.setFinishBy(sFinishBy);
                task.setFinished(false);

                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .insert(task);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                finish();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveTask st = new SaveTask();
        st.execute();
    }

    }
