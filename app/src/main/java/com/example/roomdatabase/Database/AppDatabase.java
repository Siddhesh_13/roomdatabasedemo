package com.example.roomdatabase.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.roomdatabase.Entity.Task;
import com.example.roomdatabase.Interfaces.TaskDao;


@Database(entities = {Task.class}, version = 1)

public abstract class AppDatabase extends RoomDatabase {

    public abstract TaskDao taskDao();

}
